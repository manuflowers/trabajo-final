public class RegistroAlmacen {

        private String fecha;
        private String sku;
        private int salida;
        private int ingreso;

        public String getFecha() {
            return fecha;
        }
        public void setFecha(String valfecha) {
            this.fecha = valfecha;
        }

        public void setSku(String valsku) {
            this.sku = valsku;
        }

        public String getSku() {
            return sku;
        }

        public void setSalida(int valsalida) {
            this.salida = valsalida;
        }

        public float getSalida() {
            return salida;
        }

        public int getIngreso() {
            return ingreso;
        }

        public void setIngreso(int valingreso) {
            this.ingreso = valingreso;
        }

}
