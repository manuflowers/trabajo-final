public class Productos {

    public String nombre;
    public String sku;
    public float precio;
    public int stockActual;
    public String unidad;

    public Productos (String nombre, String sku, float precio, int stockActual,String unidad){
        this.nombre=nombre;
        this.sku=sku;
        this.precio=precio;
        this.stockActual=stockActual;
        this.unidad=unidad;
    }
}
