import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
    static ArrayList <RegistroAlmacen> registrosInOut = new ArrayList <RegistroAlmacen>();
    public static void main(String[] args) {
        //LISTA DE PRODUCTOS EN ALMACEN CON STOCK INICIAL

        Productos[] productoAlmacen = new Productos[6];
        productoAlmacen[0] = new Productos("MAGNESIO", "MAG", 7, 20, "Kg");
        productoAlmacen[1] = new Productos("CALCIO", "CAL", 12, 10, "Kg");
        productoAlmacen[2] = new Productos("LITIO", "LIT", 8, 9, "Kg");
        productoAlmacen[3] = new Productos("AZUFRE", "AZU", 10, 11, "Kg");
        productoAlmacen[4] = new Productos("HIERRO", "HIE", 15, 25, "Kg");
        productoAlmacen[5] = new Productos("COBRE", "COB", 5, 22, "Kg");

        int datoOpcion;
        int indice, ingreso, salida;
        String respuesta,fecha ;

        Scanner sc2 = new Scanner(System.in);
        Scanner sc3 = new Scanner(System.in);
        do {
            datoOpcion = opcionesAlmacen(); //MUESTRA OPCIONES, DEVUELVE NUMERO ENTERO CON A OPCIÓN

            switch (datoOpcion) {

                //ACTUALIZAR EL STOCK CON EL INGRESO DE PRODUCTOS
                case 1: {
                    indice = solicitadatosIngreso(productoAlmacen);
                    if (indice != -1) {
                        System.out.print("INGRESE FECHA (dd/mm/aaaa):");
                        fecha = sc3.nextLine();
                        System.out.println("Stock  de " + productoAlmacen[indice].nombre + " es = " + productoAlmacen[indice].stockActual);
                        ingreso = solicitacantidadIngreso(productoAlmacen[indice].nombre);
                        productoAlmacen[indice].stockActual = productoAlmacen[indice].stockActual + ingreso;
                        System.out.println("Stock  actualizado de " + productoAlmacen[indice].nombre + " es = " + productoAlmacen[indice].stockActual);
                        registroMovimientos("INGRESO",fecha,ingreso,productoAlmacen[indice].sku);
                    }
                    break;
                }

                //2 - ACTUALIZAR EL STOCK CON SALIDA DE PRODUCTOS
                case 2: {
                    indice = solicitadatosSalida(productoAlmacen);
                    if (indice != -1) {
                        System.out.print("INGRESE FECHA (dd/mm/aaaa):");
                        fecha = sc3.nextLine();
                        System.out.println("Stock  de " + productoAlmacen[indice].nombre + " es = " + productoAlmacen[indice].stockActual);
                        salida = solicitacantidadSalida(productoAlmacen[indice].nombre);
                        if (salida > productoAlmacen[indice].stockActual)
                            System.out.println("El numero de Salida no puede ser mayor al Stock de " + productoAlmacen[indice].stockActual);
                        else {
                            productoAlmacen[indice].stockActual = productoAlmacen[indice].stockActual - salida;
                            System.out.println("Stock  actualizado de " + productoAlmacen[indice].nombre + " es = " + productoAlmacen[indice].stockActual);
                        }
                        registroMovimientos("SALIDA",fecha,salida,productoAlmacen[indice].sku);
                    }
                    break;
                }

                //MUESTRA INVENTARIO DE PRODUCTOS CON STOCK ACTUALIZADO
                case 3: {
                    inventarioProductos(productoAlmacen);
                    break;
                }
                //MUESTRA EL MOVIMIENTOS DE INGRESOS Y SALIDAS DEL ALMACEN
                case 4:{
                    mostrarMovimientos();
                    break;
                }

                default: {
                    System.out.println("OPCION INCORRECTA");
                }
            }
                System.out.print("CONTINUAR (S/N) : ");
                respuesta = sc2.nextLine().toUpperCase();

        }
        while (respuesta.equals("S"));
    }
    public static int opcionesAlmacen (){
        //MUESTRA OPCIONES DEL SISTEMA DE INVENTARIO

        int opcion;

        Scanner sc1 = new Scanner(System.in);
        System.out.println("SISTEMA DE INVENTARIO");
        System.out.println("---------------------");
        System.out.println("1. INGRESO DE PRODUCTOS");
        System.out.println("2. SALIDA DE PRODUCTOS");
        System.out.println("3. INVENTARIO");
        System.out.println("4. MOSTRAR MOVIMIENTOS");
        System.out.print("SELECCIONAR OPCION (1,2,3 o 4): ");

        opcion=sc1.nextInt();
        return opcion;
    }
    private static int solicitadatosIngreso(Productos[] prodAlmacen) {
        //SOLICITA INFORMACIÓN PARA INGRESO DE UN PRODUCTO
        String nombreProducto;

        Scanner sc1 = new Scanner(System.in);
        System.out.println("INGRESO DE PRODUCTOS");
        System.out.println("--------------------");
        System.out.print("NOMBRE DE PRODUCTO: ");
        nombreProducto=sc1.nextLine().toUpperCase();
        System.out.println(nombreProducto);

        //VERIFICAR EXISTENCIA DE UN PRODUCTO
        int resp= verificaProducto(prodAlmacen,nombreProducto);
        if (resp==-1) {
            System.out.println("PRODUCTO NO EXISTE");
        }
        return resp;
    }
    private static int solicitadatosSalida(Productos[] prodAlmacen) {
        //SOLICITA INFORMACIÓN PARA SALIDA DE UN PRODUCTO

        String nombreProducto;

        Scanner sc1 = new Scanner(System.in);
        System.out.println("SALIDA DE PRODUCTOS");
        System.out.println("--------------------");
        System.out.print("NOMBRE DE PRODUCTO: ");
        nombreProducto=sc1.nextLine().toUpperCase();
        System.out.println(nombreProducto);

        //VERIFICAR EXISTENCIA DE PRODUCTO
        int resp= verificaProducto(prodAlmacen,nombreProducto);

        if (resp==-1) {
            System.out.println("PRODUCTO NO EXISTE");
        }
        return resp;

    }
    private static int solicitacantidadIngreso(String nomproducto) {
        //SOLICITA CANTIDAD DE INGRESO DE PRODUCTOS
        String valor;
        boolean resp;

        do {
            Scanner sc1 = new Scanner(System.in);
            System.out.print("CANTIDAD DE INGRESO " + nomproducto + " : ");
            valor = sc1.nextLine();
            resp = isNumeric(valor);
        }while (!resp);

        return Integer.parseInt(valor);
    }
    private static int solicitacantidadSalida(String nomproducto) {
        //SOLICITA CANTIDAD DE SALIDA DE PRODUCTOS
        String valor;
        boolean resp;
        do {
            Scanner sc1 = new Scanner(System.in);
            System.out.print("CANTIDAD DE SALIDA " + nomproducto + " : ");
            valor = sc1.nextLine();
            resp = isNumeric(valor);
        } while (!resp);
        return Integer.parseInt(valor);
    }
    private static void inventarioProductos(Productos[] prodAlmacen) {

        //MUESTRA INVENTARIO CON STOCK ACTUALIZADO
        String cadena;
        int tamanocadena;
        System.out.println("NOMBRE              STOCK");
        System.out.println("===========================");
        for (int i=0; i < prodAlmacen.length;i++){

            tamanocadena = prodAlmacen[i].nombre.length();
            cadena="";

            for (int j=0;j<(20-tamanocadena);j++){
                cadena = cadena + "-";
            }
            System.out.println(prodAlmacen[i].nombre+cadena+prodAlmacen[i].stockActual+" "+prodAlmacen[i].unidad);
        }
    }
    private static int verificaProducto(Productos [] prodalmacen,String nomprod){
        //VERIFICA SI EL PRODUCTO INGRESADO ES PARTE DEL ARREGLO DE PRODUCTOS ALMACEN prodalmacen
       boolean valor = false;
       int indice = -1;
       int i=0;

       while ((!valor) && (i < prodalmacen.length)) {

           if (nomprod.equals(prodalmacen[i].nombre)) {
               valor= true;
               indice=i;
           }
           i=i+1;
       }
       return indice;
    }
    private static boolean isNumeric(String cadena){
        //VERIFICA SI LA CADENA INGRESADA ES NUMÉRICA
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe){
            return false;
        }
    }

    private static void registroMovimientos(String tipoMov,String valfecha, int valcantidad, String valsku){
        //REGISTRA MOVIMIENTOS EN UNA ARREGLO DE LA CLASE RegistroAlmacen

        RegistroAlmacen registroTemporal = new RegistroAlmacen();
        registroTemporal.setFecha(valfecha);
        registroTemporal.setSku(valsku);
        if (tipoMov.equals("INGRESO"))
            registroTemporal.setIngreso(valcantidad);
        if (tipoMov.equals("SALIDA"))
            registroTemporal.setSalida(valcantidad);

        registrosInOut.add(registroTemporal);
    }

    private static void mostrarMovimientos(){
        //MUESTRA MOVIMIENTOS REGISTRADOS EN EL ARREGLO
        Iterator<RegistroAlmacen> ptrIterator = registrosInOut.iterator();

        ptrIterator = registrosInOut.iterator();

        System.out.println("FECHA           SKU        INGRESO      SALIDA");
        System.out.println("==============================================");
        while(ptrIterator.hasNext()){
            RegistroAlmacen registroAlmacen = ptrIterator.next();
            System.out.println(registroAlmacen.getFecha()+"------"+registroAlmacen.getSku()+"---------"+registroAlmacen.getIngreso()+"------------"+registroAlmacen.getSalida());
        }

    }
}
